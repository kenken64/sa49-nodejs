require('dotenv').config();
const cors = require('cors'),
      morgan = require('morgan'),
      mysql = require('mysql'),
      bodyParser = require('body-parser'),
      express = require('express');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const APP_PORT = parseInt(process.env.PORT || 3000);
const DB_HOSTNAME = process.env.DB_HOSTNAME;
const DB_PORT = process.env.DB_PORT;
const DB_USERNAME = process.env.DB_USERNAME;
const DB_PASSWORD = process.env.DB_PASSWORD;
const DB_DATABASENAME = process.env.DB_DATABASENAME;
console.log(
    `   ${DB_HOSTNAME}
        ${DB_PORT}
        ${DB_USERNAME}
        ${DB_PASSWORD}
        ${DB_DATABASENAME}
    `
)
const SELECT_TV_SHOWS = `SELECT 
                    tvid as id, 
                    name as show_name 
                    FROM TV_SHOWS LIMIT ? OFFSET ?`;

const INSERT_TV_SHOWS = `INSERT INTO tv_shows( 
        tvid, 
        name,
        lang,
        official_site,
        rating,
        image,
        summary)
        VALUES
            (
            ?,
            ?,
            ?,
            ?,
            ?,
            ?,
            ?
        )`;

const pool = mysql.createPool(
    {
        connectionLimit : 10,
        host            : DB_HOSTNAME,
        port            : DB_PORT,
        user            : DB_USERNAME,
        password        : DB_PASSWORD,
        database        : DB_DATABASENAME
    }
);

app.use(morgan('combined'));
app.use(cors());

app.get('/api/tvshows/search', (req, res)=>{
    console.log(">>> ");
    const q = req.query;
    console.log(q);
    if(!q) {
        res.status(400).json({
            message: 'Bad request missing q'
        });
        return;
    }

    pool.getConnection( (error, conn)=>{
        if(error){
            console.warn(error);
            res.status(500).json({
                error: JSON.stringify(error),
            });
            return
        }
        console.log(`%${q}%`);
        conn.query(
            SELECT_TV_SHOWS,
            [parseInt(q.limit), parseInt(q.offset)],
            (error, result)=>{
                if(error){
                    console.warn(error);
                    res.status(500).json({
                        error: JSON.stringify(error),
                    });
                    return
                }
                res.status(200).json(result);
            }
        )
    })
    
})


app.post('/api/tvshows', (req, res)=>{
    console.log(">>> ");
    const q = req.body;
    console.log(q);
    if(!q) {
        res.status(400).json({
            message: 'Bad request missing q'
        });
        return;
    }

    pool.getConnection( (error, conn)=>{
        if(error){
            console.warn(error);
            res.status(500).json({
                error: JSON.stringify(error),
            });
            return
        }
        conn.query(
            INSERT_TV_SHOWS,
            [   parseInt(q.tvid), 
                q.name, 
                q.lang , 
                q.official_site, 
                parseFloat(q.rating), 
                q.image,
                q.summary],
            (error, result)=>{
                if(error){
                    console.warn(error);
                    res.status(500).json({
                        error: JSON.stringify(error),
                    });
                    return
                }
                res.status(200).json(result);
            }
        )
    })
})

app.listen(APP_PORT, ()=>{
    console.log(`App server started at ${APP_PORT} on ${new Date()}`)
});